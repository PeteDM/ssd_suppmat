###########################
Description
###########################


The analysis reported in the paper contains a large quantity of repetitive estimation and prediction tasks which can easily be planned and distributed to a high performance computer for parallel processing. This archive contains a main_syntax.R file executing the parts of the analysis reported in section 4 of the paper:
 -- the sample size determination (model-based) simulations (see sections 4.1 to 4.4)
 -- the validation (design-based) simulations (see section 4.5)


main_syntax.R is organised as follows:
 -- Part 1: Setup and cleaning of raw data saved in the datasets directory.
 -- Part 2: Generation of R scripts and associated BASH instructions saved under the processing/batchfiles directory. This is entirely based on the array of simulations ordered in the sim_param.csv table. R and BASH files can then be batch processed by the user.
 -- Part 3: Analysis of results of the sample size determination jobs.
 -- Part 4: Analysis of results of the design-based simulations.


Columns of sim_param.csv are described in the table below:

------------------------------------------------------------------------------
Pos  Varname         Label
------------------------------------------------------------------------------
1    simrefid        Reference number uniquely identifying a simulation
                     iteration. Its format AA-BBBB-CCCC is made up of
                     parameters contained in other columns (A: simformula;
                     B: simsf * 1000; C: iterationid)
2    simformula      Reference of the model structure (one of M1; M2; M3)
3    simscenario     Title of the template R job syntax file 
                     (job_design-based.R; job_SSD.R)
4    iterationid     Number identifying individual jobs within of a set of
                     simulation iterations
5    simsf           Sampling fraction of the simulation
6    eb.mode         Flag allowing to run simplified estimation 
                     (0 = No; 1 = Yes)
7    num.threads     Number of threads to be used by INLA
8    simseed         Seed generated at random by MS Excel formula 
                     =RANDBETWEEN(1,99999999)
9    SSD.iteration   Indicator variable (0 = design-based simulation; 
                     1+ = l where l is defined in line 5 of the SSD algorithm,
                     indicating which set of theta.star to simulate a survey
                     sample from--only used for model-based SSD iterations)
10    datasource      path to the chosen sampling prior file
                     (generated in Part 1 of main_syntax.R)
------------------------------------------------------------------------------
 

Other dependency files (functions, datasets, batch processing inputs and outputs) are located in the processing directory.

These are used in batch processing and must therefore be present in the execution directory of any high performance computer used to complete the simulations.
